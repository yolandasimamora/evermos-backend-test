<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use DB;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => '017dfedd-c5dd-4ca8-9a49-749ad52ed59b',
            'name' => 'Admin',
            'email' => 'admin@mailinator.com',
            'password' => 'originalfake',

        ]);
    }
}
