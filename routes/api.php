<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Manage\CustomerController;
use App\Http\Controllers\API\OrderController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function($router){
    Route::GET('get-products', [OrderController::class, 'getProducts'])->name('get-products');
    Route::GET('get-product/{product_id}', [OrderController::class, 'getProduct'])->name('get-product');
    Route::POST('checkout', [OrderController::class, 'checkoutProduct'])->name('checkout');
    Route::POST('pay', [OrderController::class, 'payProduct'])->name('pay');
    Route::POST('cancel', [OrderController::class, 'cancelProduct'])->name('cancel');
});

