## Installation

1. git clone https://gitlab.com/yolandasimamora/evermos-backend-test.git
2. Run command composer install
3. Adjust .env file to your local environment
4. Run command php artisan key:generate
5. Run command php artisan migrate --seed

## Answers to the questions

1. The things that may happend during 12.12 sale with bad reviews are, definitely because their orders are cancelled after they checking out the goods. They really wanted it and they hope they are one more step to payment stage but it cancelled because the goods are sold. It happend because of the core logic system can not solve the process stages of buying. Order, Checkout, Payment, Done. The quantity information of the goods may be false because of the cancelled, expired or failed and then it can not be count as an available things to buy. The high traffic can be one of the reason too so the system may cause glitches.

2. Based on the case my solution is displaying the quantity of the product to customers, and do the very detailed filtration after before they can checkout the goods. After customers click buy button which means the goods is stated available on frontend, system would checking out if the quantity exists and if the cancelled, failed or expired goods on the order table exists too. So we do double checking it. I believe it will deprived the glitch of misreport of quantity.

## Requirements
1. Composer 2.2
2. PHP 7.3^

## Postman Documentation
https://documenter.getpostman.com/view/2344376/UVkqsujn
