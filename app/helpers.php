<?php

use App\Services\ApiLogService;

if ( !function_exists('api_log') )
{
    function api_log($endpoint = null, $request = null, $response = null, $user_id= null)
    {
        $apiLogService = new ApiLogService;

        return $apiLogService->store($endpoint, $request, $response, $user_id);
    }
}