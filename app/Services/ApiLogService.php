<?php

namespace App\Services;

use App;
use App\Models\ApiLog;

class ApiLogService
{   
    protected $apiLog;

    public function __construct()
    {
        $this->apiLog = App::make(App\Models\ApiLog::class);
    }

    public function store($endpoint = null)
    {
        return $this->apiLog->create([
            'endpoint' => $endpoint,
            //'request' => $request,
            //'response' => $response,
            //'customer_id' => $user_id,
            'created_at' => now(),
            'updated_at' => now(),
            'deleted_at' => now(),
        ]);
    }
}
