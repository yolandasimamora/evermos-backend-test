<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;

class CheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
        {
            return [
                'user_id' => 'required',
                'product_id' => 'required',
                'quantity' => 'required',
            ];
            
        }
    
        public function failedValidation(Validator $validator)
        {
            $errors = (new ValidationException($validator))->errors();
            // get the error messages
            $messages = [];
            // foreach with key
            foreach ($errors as $key => $value) {
                // get the error message
                $messages[$key] = $value[0];
        
            }
            // return the error messages
            throw new HttpResponseException(response()->json(['status' => 'false','message' => $messages], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
        }
}
