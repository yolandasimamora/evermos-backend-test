<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Requests\CheckoutRequest;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\PayRequest;
use DB;

class OrderController extends Controller
{

    public function getProducts()
    {
        try {

            $products = Product::all();

            if( !is_null($products) ) {

                $data = $products;

                return response()->json(['message' => 'success', 'data' => $data]);        
            }

        } catch (\Throwable $th) {

            report($th);
            $response = [
                'success' => false,
                'message' => $th->getMessage(),
            ];
    
            return response()->json($response, 400);

            api_log('api/v1/get-product', json_encode($request->all()), $response, $request->user_id);

        }
    }

    public function getProduct(Request $request)
    {
        try {

            $product = Product::where('id', $request->product_id)->first();
            
            if( !is_null($product) ) {

                $data = $product;

                return response()->json(['message' => 'success', 'data' => $data]);        
            }

        } catch (\Throwable $th) {

            report($th);
            $response = [
                'success' => false,
                'message' => 'Product not exist',
            ];
    
            return response()->json($response, 400);

            api_log('api/v1/get-product', json_encode($request->all()), $response);

        }
    }

    public function checkoutProduct(CheckoutRequest $request)
    {
        
        $product = Product::where('id', $request->product_id)->first();

        $newOrder = Order::where('product_id', '=', $request->product_id)
                            ->where('status', '=', 'cancelled')
                            ->orWhere('status', '=', 'failed')
                            ->orWhere('status', '=', 'expired')
                            ->first();
                            
        DB::beginTransaction();

        if($product->quantity != 0 && $product->quantity >= 1 && $product->status == 1){

            try
            {
                $newQuantity['quantity'] = $product->quantity - 1;
                $update = Product::where('id', $request->product_id)
                        ->update($newQuantity);
                        
                if(!empty($update))
                {
                    DB::beginTransaction();

                    try {

                        $data = [
                            'user_id' => $request->user_id,
                            'product_id' => $request->product_id,
                            'quantity' => $request->quantity,
                            'status' => '1',
                            'total_price' => $request->quantity * $product->price
                        ];
            
                        $save = Order::updateOrCreate($data);
                        
                        DB::commit();

                    } catch (\Throwable $e) {

                        DB::rollback();
                        return response()->json([
                            'status' => false,
                            'message' => $e->getMessage()
                        ], 500);

                        api_log('api/v1/checkout', json_encode($request->all()), $response, $request->user_id);

                    }
                }
                    
                DB::commit();

                return response()->json(['message' => 'success', 'data' => 'Your purchase has been added to cart']);      
                
                api_log('api/v1/checkout', json_encode($request->all()), $response, $request->user_id);
                

            } catch (\Throwable $e) {
            
                report($th);
                $response = [
                    'success' => false,
                    'message' => $e->getMessage(),
                ];
            
                return response()->json($response, 400);
            }

        } elseif(!empty($newOrder)) {

            try{

                $data['user_id'] = $request->user_id;
                $data['status'] = 'repuchased';

                $updateOrder = Order::where('id', $newOrder->id)
                    ->update($data);
                
                DB::commit();

                return response()->json(['message' => 'success', 'data' => 'Your purchase has been added to cart']);  

                api_log('api/v1/checkout', json_encode($request->all()), $response, $request->user_id);

            } catch (\Throwable $e) {
                
                report($th);
                $response = [
                    'success' => false,
                    'message' => $e->getMessage(),
                ];
            
                return response()->json($response, 400);
            }

        } else {
            $response = [
                'success' => false,
                'message' => 'Out of stock',
            ];

            api_log('api/v1/checkout', json_encode($request->all()), $response, $request->user_id);
            
            return response()->json($response, 400);
        }
        
    }

    public function payProduct(PayRequest $request)
    {

        $order = Order::where('id', $request->order_id)->first();

        DB::beginTransaction();

        if(!empty($order) && $request->payment_status == 'paid')
        {

            try{

                $data['status'] = 'paid';

                $updateOrder = Order::where('id', $order->id)
                    ->update($data);
                
                DB::commit();

                return response()->json(['message' => 'success', 'data' => 'Payment accepted']);  

            } catch (\Throwable $e) {
                
                report($th);
                $response = [
                    'success' => false,
                    'message' => $e->getMessage(),
                ];
            
                return response()->json($response, 400);
            }

        } elseif(!empty($order) && $request->payment_status == 'expired') {

            try{

                $data['status'] = 'expired';

                $updateOrder = Order::where('id', $order->id)
                    ->update($data);
                
                DB::commit();

                return response()->json(['message' => 'success', 'data' => 'Payment time expired']);  

            } catch (\Throwable $e) {
                
                report($th);
                $response = [
                    'success' => false,
                    'message' => $e->getMessage(),
                ];
            
                return response()->json($response, 400);
            }

        } elseif(!empty($order) && $request->payment_status == 'failed') {

            try{

                $data['status'] = 'failed';

                $updateOrder = Order::where('id', $order->id)
                    ->update($data);
                
                DB::commit();

                return response()->json(['message' => 'success', 'data' => 'Payment failed']);  

            } catch (\Throwable $e) {
                
                report($th);
                $response = [
                    'success' => false,
                    'message' => $e->getMessage(),
                ];
            
                return response()->json($response, 400);
            }

        }

    }

    public function cancelProduct(OrderRequest $request)
    {
        $order = Order::where('user_id', '=', $request->user_id)->where('product_id','=', $request->product_id)
        ->first();

        if(!$order) {
            return response()->json([
                'status' => false,
                'message' => "Order not found"
            ], 404);
        }

        try {

            $data['status'] = 'cancelled';
    
            $update = Order::where('id', $order->id)
                        ->update($data);

            if($update == true) {
    
                $update = Order::where('id', $order->id)
                            ->update($data);
                
                return response()->json($data);
            }

        } catch (\Throwable $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage()
            ], 500);
        }
        
    }
}
