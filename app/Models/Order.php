<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Traits\Uuids;

class Order extends Model
{
    use HasFactory, Uuids;

    protected $fillable = [
        'user_id', 'product_id', 'quantity', 'total_price', 'status', 'created_at', 'updated_at', 'deleted_at'
    ];
}
