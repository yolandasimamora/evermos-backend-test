<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApiLog extends Model
{
    use HasFactory;

    protected $fillable = [
        'endpoint', 
        'request', 
        'response', 
        'user_id',        
        'created_by',
        'created_at',
        'deleted_at'
    ];

}
